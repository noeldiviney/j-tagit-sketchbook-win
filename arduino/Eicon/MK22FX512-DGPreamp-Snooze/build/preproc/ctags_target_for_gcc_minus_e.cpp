# 1 "C:\\J-Tagit\\arduino-1.8.18\\portable\\sketchbook\\arduino\\Eicon\\MK22FX512-DGPreamp-Snooze\\MK22FX512-DGPreamp-Snooze.ino"
# 2 "C:\\J-Tagit\\arduino-1.8.18\\portable\\sketchbook\\arduino\\Eicon\\MK22FX512-DGPreamp-Snooze\\MK22FX512-DGPreamp-Snooze.ino" 2
# 16 "C:\\J-Tagit\\arduino-1.8.18\\portable\\sketchbook\\arduino\\Eicon\\MK22FX512-DGPreamp-Snooze\\MK22FX512-DGPreamp-Snooze.ino"
bool ledBlueState = false;
bool ledGreenState = false;
unsigned long now = micros();
unsigned long ledBlueHop = now;
unsigned long ledGreenHop = now;
unsigned long SleepHop = now;

SnoozeDigital digital;
SnoozeBlock config(digital);

/*-------------------------------------------------------------------------------------------*/
void setup()
/*-------------------------------------------------------------------------------------------*/
{
  // Establish serial.
  Serial1.begin(115200);
  delay(1000);

  // Configure snooze to wake on pin 15.
  // Center button of preamp board.
  Serial1.println("Setting up snoozeDigital block");
  //delay(10000);
  digital.pinMode(15, 2, 3);
}

/*-------------------------------------------------------------------------------------------*/
void loop()
/*-------------------------------------------------------------------------------------------*/
{
  now = micros();
  // Check if it is time to blink the Blue LED
  if ((now - ledBlueHop) >= 3.0 * 1000000)
  {
    if (ledBlueState == false)
    {
      Serial1.println("LED BLUE  OFF");
      digitalWrite(33 /*  26  EZ-CS-b            */, 1);
      ledBlueState = true;
    }
    else
    {
      Serial1.println("LED BLUE  ON");
      digitalWrite(33 /*  26  EZ-CS-b            */, 0);
      ledBlueState = false;
    }
    ledBlueHop = now;
  }
  // Check if it is time to blink the GREEN LED
  if ((now - ledGreenHop) >= 3.0 * 1000000)
  {
    if (ledGreenState == false)
    {
      Serial1.println("LED GREEN ON");
      digitalWrite(4 /*  29  U4-29   Test Point */, 0);
      ledGreenState = true;
    }
    else
    {
      Serial1.println("LED GREEN OFF");
      digitalWrite(4 /*  29  U4-29   Test Point */, 1);
      ledGreenState = false;
    }
    ledGreenHop = now;
  }

  /*-------------------------------------------------------------------------------------------*/
  /*  Sleep/Wakeup Test                                                                        */
  /*-------------------------------------------------------------------------------------------*/
  /*
      if ((now - SleepHop) >= SLEEP_DELAY_DURATION)
      {
          // Go to sleeep now.
          digitalWrite(PIN_CODEC_SHUTDOWN,LOW);                   // PIN 29 = CPU 55
          Serial1.println("Turning the LEDS Off");
          delay(1000);
          digitalWrite(LED_BLUE_PIN,HIGH);
    //        digitalWrite(LED_GREEN_PIN,HIGH);
          Serial1.println("Going to sleep");
          delay(1000);
          Snooze.sleep( config );

          delay(1000);
          // We just woke up.
          Serial1.println("Waking up");
          delay(500);
          Serial1.println("Still Awake");
          digitalWrite(RESET_PIN, HIGH);
          pinMode(RESET_PIN,OUTPUT);
          Serial1.println("Resetting System");
          digitalWrite(RESET_PIN, LOW);
          SleepHop = now;
      }
  */
}
