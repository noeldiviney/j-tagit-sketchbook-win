#include <Arduino.h>
#line 1 "C:\\J-Tagit\\arduino-1.8.18\\portable\\sketchbook\\arduino\\Eicon\\MK22FX512-DGPreamp-Snooze\\MK22FX512-DGPreamp-Snooze.ino"
#include <Snooze.h>

#define VERSION_MAJOR           2
#define VERSION_MINOR           0
#define VERSION_REVISION        1

#define BAUDRATE                115200
#define LED_BLUE_PIN            33  /*  26  EZ-CS-b            */
#define LED_GREEN_PIN           4   /*  29  U4-29   Test Point */
#define RESET_PIN               4   /*  29  U4-29   Test Point */
#define FF_CPU                  1000000
#define LED_BLUE_DURATION       3.0  * FF_CPU
#define LED_GREEN_DURATION      3.0  * FF_CPU
#define SLEEP_DELAY_DURATION    30   * FF_CPU

bool          ledBlueState  =   false;
bool          ledGreenState =   false;
unsigned long now           =   micros();
unsigned long ledBlueHop    =   now;
unsigned long ledGreenHop   =   now;
unsigned long SleepHop      =   now;

SnoozeDigital digital;
SnoozeBlock config(digital);

/*-------------------------------------------------------------------------------------------*/
#line 27 "C:\\J-Tagit\\arduino-1.8.18\\portable\\sketchbook\\arduino\\Eicon\\MK22FX512-DGPreamp-Snooze\\MK22FX512-DGPreamp-Snooze.ino"
void setup();
#line 42 "C:\\J-Tagit\\arduino-1.8.18\\portable\\sketchbook\\arduino\\Eicon\\MK22FX512-DGPreamp-Snooze\\MK22FX512-DGPreamp-Snooze.ino"
void loop();
#line 27 "C:\\J-Tagit\\arduino-1.8.18\\portable\\sketchbook\\arduino\\Eicon\\MK22FX512-DGPreamp-Snooze\\MK22FX512-DGPreamp-Snooze.ino"
void setup()
/*-------------------------------------------------------------------------------------------*/
{
  // Establish serial.
  Serial1.begin(115200);
  delay(1000);

  // Configure snooze to wake on pin 15.
  // Center button of preamp board.
  Serial1.println("Setting up snoozeDigital block");
  //delay(10000);
  digital.pinMode(15, INPUT_PULLUP, RISING);
}

/*-------------------------------------------------------------------------------------------*/
void loop()
/*-------------------------------------------------------------------------------------------*/
{
  now = micros();
  // Check if it is time to blink the Blue LED
  if ((now - ledBlueHop) >= LED_BLUE_DURATION)
  {
    if (ledBlueState == false)
    {
      Serial1.println("LED BLUE  OFF");
      digitalWrite(LED_BLUE_PIN, HIGH);
      ledBlueState = true;
    }
    else
    {
      Serial1.println("LED BLUE  ON");
      digitalWrite(LED_BLUE_PIN, LOW);
      ledBlueState = false;
    }
    ledBlueHop = now;
  }
  // Check if it is time to blink the GREEN LED
  if ((now - ledGreenHop) >= LED_GREEN_DURATION)
  {
    if (ledGreenState == false)
    {
      Serial1.println("LED GREEN ON");
      digitalWrite(LED_GREEN_PIN, LOW);
      ledGreenState = true;
    }
    else
    {
      Serial1.println("LED GREEN OFF");
      digitalWrite(LED_GREEN_PIN, HIGH);
      ledGreenState = false;
    }
    ledGreenHop = now;
  }

  /*-------------------------------------------------------------------------------------------*/
  /*  Sleep/Wakeup Test                                                                        */
  /*-------------------------------------------------------------------------------------------*/
  /*
      if ((now - SleepHop) >= SLEEP_DELAY_DURATION)
      {
          // Go to sleeep now.
          digitalWrite(PIN_CODEC_SHUTDOWN,LOW);                   // PIN 29 = CPU 55
          Serial1.println("Turning the LEDS Off");
          delay(1000);
          digitalWrite(LED_BLUE_PIN,HIGH);
    //        digitalWrite(LED_GREEN_PIN,HIGH);
          Serial1.println("Going to sleep");
          delay(1000);
          Snooze.sleep( config );

          delay(1000);
          // We just woke up.
          Serial1.println("Waking up");
          delay(500);
          Serial1.println("Still Awake");
          digitalWrite(RESET_PIN, HIGH);
          pinMode(RESET_PIN,OUTPUT);
          Serial1.println("Resetting System");
          digitalWrite(RESET_PIN, LOW);
          SleepHop = now;
      }
  */
}

