board=DGPreamp
boardsmanager.additional.urls=https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json,https://gitlab.com/noeldiviney/eicon_win_packages/-/raw/main/package_eicon_stm32_index.json,https://gitlab.com/noeldiviney/eicon_win_packages/-/raw/main/package_eicon_kinetis_index.json
browser=mozilla
build.path=C:\J-Tagit\arduino-1.8.18\portable\sketchbook\arduino\Eicon\MK22FX512-DGPreamp-Snooze\build
build.verbose=true
build.warn_data_percentage=75
cache.enable=true
compiler.cache_core=true
compiler.warning_level=none
console=true
console.auto_clear=true
console.error.file=stderr.txt
console.length=500
console.lines=4
console.output.file=stdout.txt
custom_keys=DGPreamp_en-us
custom_opt=DGPreamp_o2std
custom_pnum=GenF1_BLUEPILL_F103C8
custom_rtlib=GenF1_nano
custom_speed=DGPreamp_120
custom_upload=DGPreamp_openocd
custom_upload_method=GenF1_swdMethod
custom_usb=DGPreamp_serial
custom_xserial=GenF1_generic
custom_xusb=GenF1_FS
editor.antialias=true
editor.auto_close_braces=true
editor.caret.blink=true
editor.code_folding=false
editor.divider.size=0
editor.external=false
editor.font=Monospaced,plain,12
editor.indent=true
editor.invalid=false
editor.keys.alternative_cut_copy_paste=true
editor.keys.home_and_end_beginning_end_of_doc=false
editor.keys.shift_backspace_is_delete=true
editor.languages.current=
editor.linenumbers=true
editor.save_on_verify=true
editor.tabs.expand=true
editor.tabs.size=2
editor.update_extension=true
editor.window.height.default=600
editor.window.height.min=290
editor.window.width.default=500
editor.window.width.min=400
export.applet.separate_jar_files=false
export.application.fullscreen=false
export.application.platform=true
export.application.stop=true
export.delete_target_folder=true
gui.scale=auto
ide.accessible=false
last.ide.1.8.18.daterun=1639721558
last.ide.1.8.18.hardwarepath=C:\J-Tagit\arduino-1.8.18\hardware
last.screen.height=1200
last.screen.width=1920
last.sketch.count=1
last.sketch.default.location=647,209,922,838,472,0
last.sketch.default.path=C:\J-Tagit\arduino-1.8.18\portable\sketchbook\arduino\Eicon\MK22FX512-DGPreamp-Snooze\MK22FX512-DGPreamp-Snooze.ino
last.sketch0.location=647,209,922,838,472,0
last.sketch0.path=C:\J-Tagit\arduino-1.8.18\portable\sketchbook\arduino\Eicon\MK22FX512-DGPreamp-Snooze\MK22FX512-DGPreamp-Snooze.ino
last.sketch1.location=647,209,922,838,472,0
last.sketch1.path=C:\J-Tagit\arduino-1.8.18\portable\sketchbook\arduino\Eicon\MK22FX512-DGPreamp-Snooze\MK22FX512-DGPreamp-Snooze.ino
launcher=xdg-open
platform.auto_file_type_associations=true
preferences.readonly=false
preproc.color_datatype=true
preproc.enhanced_casting=true
preproc.imports.list=java.applet.*,java.awt.Dimension,java.awt.Frame,java.awt.event.MouseEvent,java.awt.event.KeyEvent,java.awt.event.FocusEvent,java.awt.Image,java.io.*,java.net.*,java.text.*,java.util.*,java.util.zip.*,java.util.regex.*
preproc.output_parse_tree=false
preproc.save_build_files=false
preproc.substitute_floats=true
preproc.substitute_unicode=true
preproc.web_colors=true
programmer=arduino:avrispmkii
proxy.manual.hostname=
proxy.manual.password=
proxy.manual.port=
proxy.manual.type=HTTP
proxy.manual.username=
proxy.pac.url=
proxy.type=auto
recent.sketches=C:\J-Tagit\arduino-1.8.18\portable\sketchbook\arduino\Eicon\MK22FX512-DGPreamp-Snooze\MK22FX512-DGPreamp-Snooze.ino
run.display=1
run.options=
run.options.memory=false
run.options.memory.initial=64
run.options.memory.maximum=256
run.present.bgcolor=#666666
run.present.exclusive=false
run.present.stop.color=#cccccc
serial.databits=8
serial.debug_rate=9600
serial.line_ending=1
serial.parity=N
serial.stopbits=1
sketchbook.path=C:\J-Tagit\arduino-1.8.18\portable\sketchbook
software=ARDUINO
target_package=Eicon
target_platform=kinetis
theme.file=
update.check=true
update.id=-3249230128592374948
update.last=1639720149687
upload.using=bootloader
upload.verbose=true
upload.verify=true
