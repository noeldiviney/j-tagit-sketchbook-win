# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/DinRDuino/arduino-1.8.18/portable/sketchbook/cmake/Eicon/F103C8T6/BluePill-HelloW-V1.0.1/BluePill-HelloW-V1.0.1.cpp" "C:/DinRDuino/arduino-1.8.18/portable/sketchbook/cmake/Eicon/F103C8T6/BluePill-HelloW-V1.0.1/build/CMakeFiles/BluePill-HelloW-V1.0.1.elf.dir/BluePill-HelloW-V1.0.1.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ARDUINO_ARCH_"
  "ARDUINO_F103C8T6_BluePill_HelloW"
  "ARDUINO_VERSION_2"
  "BOARD_NAME=\"F103C8T6_BluePill_HelloW.h\""
  "HAL_UART_MODULE_ENABLED"
  "STM32F103xB"
  "STM32F1xx"
  "VARIANT_H=\"variant_F103C8T6_BluePill_HelloW.h\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "C:/DinRDuino/arduino_1.8.18/portable/sketchbook/cmake/Eicon/F103C8T6/BluePill-HelloW-V1.0.1/BluePill-HelloW-V1.0.1"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/variants/STM32F1xx/F103C8T6_BluePill_HelloW"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/cores/arduino"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/cores/arduino/avr"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/cores/arduino/stm32"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/cores/arduino/stm32/LL"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/cores/arduino/stm32/usb"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/cores/arduino/stm32/usb/hid"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/cores/arduino/stm32/usb/cdc"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/system/Drivers/STM32F1xx_HAL_Driver/Inc"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/system/Drivers/STM32F1xx_HAL_Driver/Src"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/system/Drivers/CMSIS/Device/ST/STM32F1xx/Include"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/system/Drivers/CMSIS/Device/ST/STM32F1xx/Source/Templates/gcc"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/system/STM32F1xx"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/system/Middlewares/ST/STM32_USB_Device_Library/Core/Inc"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/system/Middlewares/ST/STM32_USB_Device_Library/Core/Src"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/hardware/STM32/2.2.0/libraries/SrcWrapper/src"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/tools/CMSIS/CMSIS/Core/Include"
  "C:/DinRDuino/arduino-1.8.18/portable/packages/Eicon/tools/CMSIS/CMSIS/DSP/Include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "C:/DinRDuino/arduino-1.8.18/portable/sketchbook/cmake/Eicon/F103C8T6/BluePill-HelloW-V1.0.1/build/CMakeFiles/arduinolib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
