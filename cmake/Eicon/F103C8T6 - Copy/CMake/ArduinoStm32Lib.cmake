SET(CCLL                                "CMAKE_CURRENT_LIST_LINE")

#---------------  project Definition  -----------------------------------------------------------------------------------------#
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     -----------------------------------------------------------------------------")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     Processing                     = ArduinoStm32Lib.cmake")


MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     MCU_LINKER_SCRIPT              = ${MCU_LINKER_SCRIPT}")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}     ARDUINO_VERSION                = ${ARDUINO_VERSION}")

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    CMAKE_C_COMPILER               = ${CMAKE_C_COMPILER}")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    CMAKE_CXX_COMPILER             = ${CMAKE_CXX_COMPILER}")


#MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    return()                       ")
#return()

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    objcopy                        = ${CMAKE_OBJCOPY}")

#MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    CMAKE_LINKER_FLAGS           = ${CMAKE_EXE_LINKER_FLAGS}")
#MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    CMAKE_LINKER_FLAGS           = ${CMAKE_LINKER_FLAGS}")

#---------------  Add Definitions  --------------------------------------------------------------------------------------------#

if(FALSE) # fake a block comment
# -DSTM32F4xx -DARDUINO=10812 -DARDUINO_F427ZIT6_DRD_Blink -DARDUINO_ARCH_STM32 "-DBOARD_NAME=\"F427ZIT6_DRD_Blink\"" -DSTM32F427xx -DHAL_UART_MODULE_ENABLED
add_definitions(-D${MCU_LINE})
add_definitions(-DUSE_HAL_LIBRARY)
add_definitions(-D${MCU_FAMILY})
add_definitions(-DARDUINO=${ARDUINO_VERSION})
add_definitions(-DARDUINO_${EICON_BOARD})
add_definitions(-DARDUINO_ARCH_STM32)
add_definitions(-DBOARD_NAME=${EICON_BOARD})
add_definitions(-D${BOARD_TYPE})
add_definitions(-DHAL_UART_MODULE_ENABLED)

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -D${MCU_LINE}")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -DUSE_HAL_LIBRARY") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -D${MCU_FAMILY}") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -D${ARDUINO_VERSION}") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -DARDUINO_${EICON_BOARD}") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -DARDUINO_ARCH_STM32") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -DBOARD_NAME=${EICON_BOARD}") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -D${BOARD_TYPE}") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -DHAL_UART_MODULE_ENABLED") 
endif()


add_definitions(-DSTM32F4xx)
add_definitions(-DARDUINO=10812)
add_definitions(-DARDUINO_F427ZIT6_DRD_Blink)
add_definitions(-DARDUINO_ARCH_STM32)
add_definitions("-DBOARD_NAME=\"F427ZIT6_DRD_Blink\"")
add_definitions(-DSTM32F427xx)
add_definitions(-DHAL_UART_MODULE_ENABLED)

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -DSTM32F4xx")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -DARDUINO=10812") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -DARDUINO_F427ZIT6_DRD_Blink") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -DARDUINO_ARCH_STM32") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -DBOARD_NAME=\"F427ZIT6_DRD_Blink\"") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -DARDUINO_ARCH_STM32") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -DSTM32F427xx") 
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    Adding Definitions             = -DHAL_UART_MODULE_ENABLED") 

#---------------  Add Sources  ------------------------------------------------------------------------------------------------#
if(FALSE) # fake a block comment
file(GLOB_RECURSE SKETCH_CPP_SOURCES 		${SKETCH_PATH}/*.cpp)

file(GLOB_RECURSE VARIANTS_CPP_SOURCES    	${VARIANTS_PATH}/${BOARD_VARIANT}/*.cpp)
file(GLOB_RECURSE VARIANTS_C_SOURCES    	${VARIANTS_PATH}/${BOARD_VARIANT}/*.c)
file(GLOB_RECURSE STARTUP_SOURCES               ${STM32_PLATFORM_PATH}/cores/arduino/stm32/*.S)
file(GLOB_RECURSE ARDUINO_CPP_SOURCES    	${STM32_PLATFORM_PATH}/cores/arduino/*.cpp)
file(GLOB_RECURSE ARDUINO_C_SOURCES    		${STM32_PLATFORM_PATH}/cores/arduino/*.c)
file(GLOB_RECURSE AVR_C_SOURCES    		${STM32_PLATFORM_PATH}/cores/arduino/avr/*.c)
file(GLOB_RECURSE STM32_C_SOURCES    		${STM32_PLATFORM_PATH}/cores/arduino/stm32/*.c)
file(GLOB_RECURSE STM32_HAL_C_SOURCES    	${STM32_PLATFORM_PATH}/cores/arduino/stm32/HAL/*.c)
file(GLOB_RECURSE STM32_LL_C_SOURCES    	${STM32_PLATFORM_PATH}/cores/arduino/stm32/LL/*.c)
file(GLOB_RECURSE STM32_USB_C_SOURCES    	${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/*.c)
file(GLOB_RECURSE STM32_USB_HID_C_SOURCES   ${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/hid/*.c)
file(GLOB_RECURSE STM32_USB_CDC_C_SOURCES   ${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/cdc/*.c)

set(ARDUINOLIB_SOURCES ${STARTUP_SOURCES} ${VARIANTS_CPP_SOURCES} ${ARDUINO_CPP_SOURCES} ${VARIANTS_C_SOURCES} ${ARDUINO_C_SOURCES} ${AVR_C_SOURCES} ${STM32_C_SOURCES})
set(ARDUINOLIB_SOURCES ${ARDUINOLIB_SOURCES} ${STM32_HAL_C_SOURCES} ${STM32_LL_C_SOURCES} ${STM32_USB_C_SOURCES} ${STM32_USB_HID_C_SOURCES} ${STM32_USB_CDC_C_SOURCES})
set(APP_SOURCE_FILES ${SKETCH_SOURCES} ${MCU_LINKER_SCRIPT})

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD SKETCH_CPP_SOURCES       = file(GLOB_RECURSE SKETCH_SOURCES            ${SKETCH_PATH}/*.cpp)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD VARIANTS_CPP_SOURCES     = file(GLOB_RECURSE VARIANTS_CPP_SOURCES      ${VARIANTS_PATH}/${BOARD_VARIANT}/*.cpp)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD VARIANTS_C_SOURCES       = file(GLOB_RECURSE VARIANTS_C_SOURCES        ${VARIANTS_PATH}/${BOARD_VARIANT}/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD STARTUP_SOURCES          = file(GLOB_RECURSE STARTUP_SOURCES           ${STM32_PLATFORM_PATH}/cores/arduino/stm32/*.S)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD ARDUINO_CPP_SOURCES      = file(GLOB_RECURSE ARDUINO_CPP_SOURCES       ${STM32_PLATFORM_PATH}/cores/arduino/*.cpp)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD ARDUINO_C_SOURCES        = file(GLOB_RECURSE ARDUINO_C_SOURCES         ${STM32_PLATFORM_PATH}/cores/arduino/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD AVR_C_SOURCES            = file(GLOB_RECURSE AVR_C_SOURCES             ${STM32_PLATFORM_PATH}/cores/arduino/avr/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD STM32_C_SOURCES          = file(GLOB_RECURSE STM32_C_SOURCES           ${STM32_PLATFORM_PATH}/cores/arduino/stm32/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD STM32_HAL_C_SOURCES      = file(GLOB_RECURSE STM32_HAL_C_SOURCES       ${STM32_PLATFORM_PATH}/cores/arduino/stm32/HAL/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD STM32_LL_C_SOURCES       = file(GLOB_RECURSE STM32_HAL_C_SOURCES       ${STM32_PLATFORM_PATH}/cores/arduino/stm32/LL/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD STM32_USB_C_SOURCES      = file(GLOB_RECURSE STM32_USB_C_SOURCES       ${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD STM32_USB_HID_C_SOURCES  = file(GLOB_RECURSE STM32_USB_HID_C_SOURCES   ${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/hid/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD STM32_USB_CDC_C_SOURCES  = file(GLOB_RECURSE STM32_USB_CDC_C_SOURCES   ${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/cdc/*.c)")

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}    ADD ALL Source files to      = 'SOURCE_FILES' Variable")
endif()


#---------------  Add Sources  ------------------------------------------------------------------------------------------------#

file(GLOB_RECURSE SKETCH_CPP_SOURCES 		${SKETCH_PATH}/*.cpp)

file(GLOB_RECURSE VARIANTS_CPP_SOURCES    	${VARIANTS_PATH}/${BOARD_VARIANT}/*.cpp)
file(GLOB_RECURSE VARIANTS_C_SOURCES    	${VARIANTS_PATH}/${BOARD_VARIANT}/*.c)

file(GLOB_RECURSE STARTUP_SOURCES               ${STM32_PLATFORM_PATH}/cores/arduino/stm32/*.S)
file(GLOB_RECURSE ARDUINO_C_SOURCES    		${STM32_PLATFORM_PATH}/cores/arduino/*.c)
file(GLOB_RECURSE ARDUINO_CPP_SOURCES    	${STM32_PLATFORM_PATH}/cores/arduino/*.cpp)
file(GLOB_RECURSE AVR_C_SOURCES    		${STM32_PLATFORM_PATH}/cores/arduino/avr/*.c)
file(GLOB_RECURSE STM32_USB_C_SOURCES    	${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/*.c)
file(GLOB_RECURSE STM32_USB_HID_C_SOURCES       ${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/hid/*.c)
file(GLOB_RECURSE STM32_USB_CDC_C_SOURCES       ${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/cdc/*.c)
file(GLOB_RECURSE SRCWRAPPER_C_SOURCES          ${STM32_PLATFORM_PATH}/libraries/SrcWrapper/*.c)
file(GLOB_RECURSE SRCWRAPPER_HAL_C_SOURCES    	${STM32_PLATFORM_PATH}/libraries/SrcWrapper/src/HAL/*.c)
file(GLOB_RECURSE SRCWRAPPER_LL_C_SOURCES    	${STM32_PLATFORM_PATH}/libraries/SrcWrapper/src/LL/*.c)
file(GLOB_RECURSE SRCWRAPPER_STM32_C_SOURCES    ${STM32_PLATFORM_PATH}/libraries/SrcWrapper/src/stm32/*.c)
file(GLOB_RECURSE SRCWRAPPER_STM32_CPP_SOURCES  ${STM32_PLATFORM_PATH}/libraries/SrcWrapper/src/stm32/*.cpp)

set(ARDUINOLIB_SOURCES ${VARIANTS_CPP_SOURCES} ${VARIANTS_C_SOURCES} ${STARTUP_SOURCES} ${ARDUINO_CPP_SOURCES} ${ARDUINO_C_SOURCES} ${AVR_C_SOURCES} ${STM32_USB_C_SOURCES} ${STM32_USB_HID_C_SOURCES})
set(ARDUINOLIB_SOURCES ${ARDUINOLIB_SOURCES} ${STM32_USB_CDC_C_SOURCES} ${SRCWRAPPER_C_SOURCES} ${SRCWRAPPER_HAL_C_SOURCES} ${SRCWRAPPER_LL_C_SOURCES} ${SRCWRAPPER_STM32_C_SOURCES} ${SRCWRAPPER_STM32_CPP_SOURCES}) 
set(APP_SOURCE_FILES ${SKETCH_SOURCES} ${MCU_LINKER_SCRIPT})

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD SKETCH_CPP_SOURCES         = file(GLOB_RECURSE SKETCH_SOURCES              ${SKETCH_PATH}/*.cpp)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD VARIANTS_CPP_SOURCES       = file(GLOB_RECURSE VARIANTS_CPP_SOURCES        ${VARIANTS_PATH}/${BOARD_VARIANT}/*.cpp)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD VARIANTS_C_SOURCES         = file(GLOB_RECURSE VARIANTS_C_SOURCES          ${VARIANTS_PATH}/${BOARD_VARIANT}/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD STARTUP_SOURCES            = file(GLOB_RECURSE STARTUP_SOURCES             ${STM32_PLATFORM_PATH}/cores/arduino/stm32/*.S)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD ARDUINO_C_SOURCES          = file(GLOB_RECURSE ARDUINO_C_SOURCES           ${STM32_PLATFORM_PATH}/cores/arduino/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD ARDUINO_CPP_SOURCES        = file(GLOB_RECURSE ARDUINO_CPP_SOURCES         ${STM32_PLATFORM_PATH}/cores/arduino/*.cpp)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD AVR_C_SOURCES              = file(GLOB_RECURSE AVR_C_SOURCES               ${STM32_PLATFORM_PATH}/cores/arduino/avr/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD STM32_USB_C_SOURCES        = file(GLOB_RECURSE STM32_USB_C_SOURCES         ${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD STM32_USB_HID_C_SOURCES    = file(GLOB_RECURSE STM32_USB_HID_C_SOURCES     ${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/hid/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD STM32_USB_CDC_C_SOURCES    = file(GLOB_RECURSE STM32_USB_CDC_C_SOURCES     ${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/cdc/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD SRCWRAPPER_C_SOURCES       = file(GLOB_RECURSE SRCWRAPPER_C_SOURCES        ${STM32_PLATFORM_PATH}/libraries/SrcWrapper/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD SRCWRAPPER_HAL_C_SOURCES   = file(GLOB_RECURSE SRCWRAPPER_HAL_C_SOURCES    ${STM32_PLATFORM_PATH}/libraries/SrcWrapper/src/HAL/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD SRCWRAPPER_LL_C_SOURCES    = file(GLOB_RECURSE SRCWRAPPER_LL_C_SOURCES     ${STM32_PLATFORM_PATH}/libraries/SrcWrapper/src/LL/*.c)")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD SRCWRAPPER_STM32_C_SOURCES = file(GLOB_RECURSE SRCWRAPPER_STM32_C_SOURCES  ${STM32_PLATFORM_PATH}/libraries/SrcWrapper/src/LL/*.c)")

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ADD ALL Source files to        = 'SOURCE_FILES' Variable")


#---------------  Add Include Directories  -------------------------------------------------------------------------------------#

#include_directories(${SKETCH_PATH})
include_directories(${SKETCH_PATH}/${PROJECT_NAME})
include_directories(${VARIANTS_PATH}/${BOARD_VARIANT})
include_directories(${STM32_PLATFORM_PATH}/cores/arduino)
include_directories(${STM32_PLATFORM_PATH}/cores/arduino/avr)
include_directories(${STM32_PLATFORM_PATH}/cores/arduino/stm32)
include_directories(${STM32_PLATFORM_PATH}/cores/arduino/stm32/LL)
include_directories(${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb)
include_directories(${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/hid)
include_directories(${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/cdc)
include_directories(${STM32_PLATFORM_PATH}/system/Drivers/${MCU_FAMILY}_HAL_Driver/Inc/)
include_directories(${STM32_PLATFORM_PATH}/system/Drivers/${MCU_FAMILY}_HAL_Driver/Src/)
include_directories(${STM32_PLATFORM_PATH}/system/Drivers/CMSIS/Device/ST/${MCU_FAMILY}/Include/)
include_directories(${STM32_PLATFORM_PATH}/system/Drivers/CMSIS/Device/ST/${MCU_FAMILY}/Source/Templates/gcc/)
include_directories(${STM32_PLATFORM_PATH}/system/${MCU_FAMILY}/)
include_directories(${STM32_PLATFORM_PATH}/system/Middlewares/ST/STM32_USB_Device_Library/Core/Inc)
include_directories(${STM32_PLATFORM_PATH}/system/Middlewares/ST/STM32_USB_Device_Library/Core/Src)
include_directories(${STM32_PLATFORM_PATH}/libraries/SrcWrapper/src)
include_directories(${STM32_TOOLS_PATH}/CMSIS/5.5.1/CMSIS/Core/Include/)
include_directories(${STM32_TOOLS_PATH}/CMSIS/5.5.1/CMSIS/DSP/Include/)
#include_directories(/mnt/${DRV}/Arduino/hardware/${BOARD_MANUFACTURER}/stm32/1.6.1/variants/DinRDuino_F427ZIT6)

MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ---------------------------------------------------------------------------------")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Directories")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${SKETCH_PATH}/${PROJECT_NAME}")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${VARIANTS_PATH}/${BOARD_VARIANT}")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/cores/arduino")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/cores/arduino/avr")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/cores/arduino/stm32")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/cores/arduino/stm32/LL")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/hid")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/cores/arduino/stm32/usb/cdc")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/system/Drivers/${MCU_FAMILY}_HAL_Driver/Inc/")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/system/Drivers/${MCU_FAMILY}_HAL_Driver/Src/")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/system/Drivers/CMSIS/Device/ST/${MCU_FAMILY}/Include/")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/system/Drivers/CMSIS/Device/ST/${MCU_FAMILY}/Source/Templates/gcc/")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/system/${MCU_FAMILY}/")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/system/Middlewares/ST/STM32_USB_Device_Library/Core/Inc")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/system/Middlewares/ST/STM32_USB_Device_Library/Core/Src")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_PLATFORM_PATH}/libraries/SrcWrapper/src")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_TOOLS_PATH}/CMSIS/5.5.1/CMSIS/Core/Include/")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add Include Dirs               = ${STM32_TOOLS_PATH}/CMSIS/5.5.1/CMSIS/DSP/Include/")

#---------------  Add Library Definitions  ----------------------------------------------------------------------------------------#
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ---------------------------------------------------------------------------------")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   Add arduinolib")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ARDUINOLIB_SOURCES             = STARTUP_SOURCES VARIANTS_CPP_SOURCES ARDUINO_CPP_SOURCES VARIANTS_C_SOURCES ARDUINO_C_SOURCES ")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ARDUINOLIB_SOURCES contd       = AVR_C_SOURCES STM32_C_SOURCES ARDUINOLIB_SOURCES STM32_HAL_C_SOURCES STM32_LL_C_SOURCES ")
MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   ARDUINOLIB_SOURCES contd       = STM32_USB_C_SOURCES STM32_USB_HID_C_SOURCES STM32_USB_CDC_C_SOURCES")


MESSAGE(STATUS "ArduinoStm32Lib.cmake line ${${CCLL}}   All done                       = Arduino.cmake")
 
#---------------  All Done  ---------------------------------------------------------------------------------------------------#

